import os

import networkx as nx
from luigi import Task
from luigi.task import flatten
from luigi_task_helpers import get_folder_path
import pygraphviz


class RawTelematics(Task):
    def requires(self):
        pass


class POIWebscrape(Task):
    def requires(self):
        pass


class GetOSMShape(Task):
    def requires(self):
        pass


class POIShape(Task):
    def requires(self):
        return {
            'POI_Locations': self.clone(POIWebscrape),
            'osm_shape': self.clone(GetOSMShape)
        }


class PreprocessTelematicsStops(Task):
    def requires(self):
        return {
            "telematics": self.clone(RawTelematics)
        }


class TelematicsStopsNearPOI(Task):
    def requires(self):
        return {
            "telematics": self.clone(PreprocessTelematicsStops),
            "shape": self.clone(POIShape)
        }


class CensusData(Task):
    def requires(self):
        pass


class UserAddresses(Task):
    def requires(self):
        return {
            "address": self.clone(PreprocessTelematicsStops)
        }


class POIAnalysis(Task):
    def requires(self):
        return {
            "address": self.clone(UserAddresses),
            "census": self.clone(CensusData),
            "stops": self.clone(TelematicsStopsNearPOI)
        }


def draw_recursive(task, graph):
    for i in flatten(task.requires()):
        draw_recursive(i, graph)
        graph.add_edges_from([(i.__class__.__name__, task.__class__.__name__)])


if __name__ == '__main__':
    test_graph = nx.DiGraph()
    draw_recursive(POIAnalysis(), test_graph)
    A = nx.nx_agraph.to_agraph(test_graph)
    A.layout('dot', args='-Nfontsize=15 -Nwidth=".2" -Nheight=".2" -Nmargin=0 -Gfontsize=8')
    try:
        A.draw(os.path.join(get_folder_path(), "src/visualization/solemetrics_task_graph.png"))
    except:
        print("Try brew installing graphviz!")
