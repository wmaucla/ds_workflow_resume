import json
import os

import requests
from bs4 import BeautifulSoup
from luigi import Task, build

from luigi_task_helpers import get_folder_path, salted_target


class WebscrapeAMC(Task):

    __version__ = '1.0'

    def run(self):

        main_website = requests.get("https://www.amctheatres.com/movie-theatres")  # get everything on the home page
        main_soup = BeautifulSoup(main_website.content, 'html.parser')
        t = main_soup.find_all("a", class_="Link Link--arrow Link--reversed Link--arrow--txt--tiny txt--tiny",
                               href=True)

        theater_addresses = []
        for i in range(len(t)):  # step through each of the cities/state
            website = requests.get("https://www.amctheatres.com/" + t[i]['href'])
            soup = BeautifulSoup(website.content, 'html.parser')
            theater_addresses.append(
                [" ".join(theater.text.split()) for theater in soup.find_all("span", class_="Link-text txt--thin")])

        all_addresses = [item for i in theater_addresses for item in i]
        print(len(all_addresses))

        with open(self.output().path, 'w') as f:
            f.write(json.dumps(all_addresses))

    def output(self):
        return salted_target(
            self, os.path.join(get_folder_path(), 'data/processed/webscraped_amc_addresses' + "-{salt}.txt"))


if __name__ == '__main__':
    webscraping_task = WebscrapeAMC()

    build([
        webscraping_task
    ], local_scheduler=True)
