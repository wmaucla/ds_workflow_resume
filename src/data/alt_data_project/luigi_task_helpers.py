from hashlib import sha256

from luigi import LocalTarget
from luigi.task import flatten
import os
import re


def get_folder_path():
    return re.match(r'(.*)?/ds_workflow_resume/', os.path.dirname(__file__)).group(0)


def get_salted_version(task):
    """Create a salted id/version for this task and lineage
    Credit to Dr Scott Gorlin: https://github.com/gorlins/salted

    :returns: a unique, deterministic hexdigest for this task
    :rtype: str
    """

    msg = ""

    # Salt with lineage
    for req in flatten(task.requires()):
        # Note that order is important and impacts the hash - if task
        # requirements are a dict, then consider doing this is sorted order
        msg += get_salted_version(req)

    # Uniquely specify this task
    msg += ','.join([

                        # Basic capture of input type
                        task.__class__.__name__,

                        # Change __version__ at class level when everything needs rerunning!
                        task.__version__,

                    ] + [
                        # Depending on strictness - skipping params is acceptable if
                        # output already is partitioned by their params; including every
                        # param may make hash *too* sensitive
                        '{}={}'.format(param_name, repr(task.param_kwargs[param_name]))
                        for param_name, param in sorted(task.get_params())
                        if param.significant
                    ]
                    )
    return sha256(msg.encode()).hexdigest()


# noinspection PyShadowingBuiltins
def salted_target(task, file_pattern, format=None, **kwargs):
    """A local target with a file path formed with a 'salt' kwarg

    :rtype: LocalTarget
    """
    return LocalTarget(file_pattern.format(
        salt=get_salted_version(task)[:6], self=task, **kwargs
    ), format=format)