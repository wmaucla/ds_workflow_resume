import os
import random

import folium
import geopandas as gpd
from luigi import Task, build
from luigi_task_helpers import salted_target, get_folder_path
from scipy.spatial import cKDTree
from shapely.geometry import Point


class CreateHomeDepotLot(Task):

    __version__ = '1.0'

    def run(self):

        out_file = self.output().path
        parking_lot = gpd.read_file("../../../data/external/home_depot_parking_lot.geojson")

        random.seed(42)

        # Simulate a bunch of points within the parking lot
        random_lot_list = []
        lot_bounds = parking_lot['geometry'][0].bounds
        random.seed(42)
        for k in range(random.randint(0, 2000)):
            lat = random.uniform(lot_bounds[1], lot_bounds[3])
            lng = random.uniform(lot_bounds[0], lot_bounds[2])
            if Point(lng, lat).within(parking_lot['geometry'][0]):
                random_lot_list.append((lat, lng))

        store_locations = [(42.3265, -71.0652), (42.3273, -71.0642), (42.3282, -71.0638), (42.3293, -71.0632),
                           (42.3279, -71.0609), (42.3276, -71.0613), (42.3272, -71.0616), (42.3269, -71.0618),
                           (42.3264, -71.0621)]  # cheating a bit; but can use google api to scrape nearby locations

        voronoi_kdtree = cKDTree(store_locations)
        test_point_dist, test_point_regions = voronoi_kdtree.query(random_lot_list, k=1)
        colors = {0: "#a6cee3", 1: "#1f78b4", 2: "#b2df8a", 3: "#33a02c",
                  4: "#fb9a99", 5: "#e31a1c", 6: "#fdbf6f", 7: "#ff7f00", 8: "#cab2d6"}

        hd_map = folium.Map(
            location=[42.325596, -71.062862],
                zoom_start=16,
            )
        folium.LatLngPopup().add_to(hd_map)

        for i in range(len(random_lot_list)):  # add in the points and the corresponding parking lot
            folium.CircleMarker(random_lot_list[i], color=colors[test_point_regions[i]], radius=1).add_to(hd_map)

        for i in range(len(store_locations)):
            folium.Marker(store_locations[i]).add_to(hd_map)

        hd_map.save(out_file)

    def output(self):
        return salted_target(
            self, os.path.join(get_folder_path(),
                               'src/visualization/home_depot_parking_lot-{salt}.html'))

if __name__ == '__main__':
    hd_lot = CreateHomeDepotLot()

    build([
        hd_lot
    ], local_scheduler=True)
