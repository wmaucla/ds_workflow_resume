import os
import random
from random import shuffle

import folium
import geopandas as gpd
import numpy as np
import pandas as pd
from folium.plugins import HeatMap
from luigi import Task, build
from luigi_task_helpers import salted_target, get_folder_path
from sklearn.cluster import DBSCAN


class ClusteringDemo(Task):

    __version__ = '1.0'

    def run(self):

        out_file = self.output().path
        parking_lots = gpd.read_file("../../../data/external/white_plains_parking_lots.geojson")

        random.seed(42)

        # Simulate a bunch of random points near a home
        random_points_list = []
        for i in range(100):
            random_points_list.append((random.uniform(41.038946, 41.039735),
                                       random.uniform(-73.782359, -73.781426)))

        for i in range(50):
            random_points_list.append((random.uniform(41.033762, 41.034434),
                                       random.uniform(-73.758144, -73.757262)))

        # Simulate a bunch of points within the parking lot
        for j in range(len(parking_lots)):
            lot_bounds = parking_lots['geometry'][j].bounds
            for k in range(random.randint(0, 10)):
                lat = random.uniform(lot_bounds[1], lot_bounds[3])
                lng = random.uniform(lot_bounds[0], lot_bounds[2])
                random_points_list.append((lat, lng))

        shuffle(random_points_list)

        stop_map = folium.Map(
            location=[41.038946, -73.782359],
            zoom_start=14, tiles="CartoDB positron"
        )

        HeatMap(random_points_list, radius=3,
                blur=1, name="All Points").add_to(stop_map)  # add in the map with all the telematics points

        clustering = DBSCAN(eps=0.03 / 6371.0088,
                            min_samples=10, algorithm='ball_tree',
                            metric='haversine').fit(np.radians(random_points_list))

        test_data = pd.DataFrame({'lat': [i[0] for i in random_points_list],
                                  'lng': [i[1] for i in random_points_list],
                                  'cluster': clustering.labels_
                                  })

        HeatMap(test_data[test_data['cluster'] == 0][['lat', 'lng']].values,
                radius=3, blur=1, name="Home Estimated Points").add_to(
            stop_map)

        folium.LayerControl().add_to(stop_map)

        stop_map.save(out_file)

    def output(self):
        return salted_target(
            self, os.path.join(get_folder_path(),
                               'src/visualization/clustering-{salt}.html'))


if __name__ == '__main__':
    clustering = ClusteringDemo()

    build([
        clustering
    ], local_scheduler=True)
