import os
import random
import re
import timeit
from datetime import date
from datetime import datetime, timedelta
from random import choices

import geohash_hilbert as ghh
import geopandas as gpd
import pandas as pd
from luigi import DateIntervalParameter, Task, build
from luigi.date_interval import Week
from luigi_task_helpers import salted_target, get_folder_path
from shapely.geometry import Point


class GenerateFakeTelematics(Task):

    date_interval = DateIntervalParameter()

    __version__ = '1.0'

    def run(self):

        with self.output().open('w') as out_file:
            all_lot = gpd.read_file(os.path.join(get_folder_path(), "data/external/south_shore_all.geojson"))
            parking_lots = gpd.read_file(os.path.join(get_folder_path(),
                                                      "data/external/south_shore_parking_lot.geojson"))
            random.seed(42)

            # Simulate a bunch of random points on the roads
            all_lines_str = all_lot['geometry'].values
            random_points_list = []
            for i in range(3000):
                random_coords = all_lines_str[random.randint(0, len(all_lines_str) - 1)].coords[:]
                random_points_list.append(random_coords[random.randint(0, len(random_coords) - 1)])

            # Simulate a bunch of points within the parking lot
            random_lot_list = []
            for j in range(len(parking_lots)):
                lot_bounds = parking_lots['geometry'][j].bounds
                for k in range(random.randint(0, 100)):
                    lat = random.uniform(lot_bounds[1], lot_bounds[3])
                    lng = random.uniform(lot_bounds[0], lot_bounds[2])
                    random_lot_list.append((lng, lat))

            random_points_list.extend(random_lot_list)
            df = pd.DataFrame(random_points_list, columns=['lng', 'lat'])
            df['date'] = choices(self.date_interval.dates(), k=len(random_points_list))
            df['time'] = [(datetime.now() +
                           k * timedelta(random.randint(0, 100), random.randint(0, 100))).strftime('%H:%M:%S')
                          for k in range(len(df))]
            df['geohash'] = df.apply(lambda x: ghh.encode(x['lng'], x['lat'], precision=6), axis=1)

            df.to_csv(out_file, index=False)

    def output(self):
        return salted_target(
            self, os.path.join(get_folder_path(),
                               'data/raw/fake_telematics_data'.format(self.date_interval) + "-{salt}.csv"))


class SpatialJoinTelematics(Task):

    date_interval = DateIntervalParameter()

    __version__ = '1.1'

    def requires(self):
        return {
            "raw_telematics": GenerateFakeTelematics(date_interval=self.date_interval)
        }

    def run(self):

        file_path = self.requires()['raw_telematics'].output().path
        pnts = pd.read_csv(file_path).set_index(['geohash'])
        parking_lots = gpd.read_file(os.path.join(get_folder_path(), "data/external/south_shore_parking_lot.geojson"))

        new_set = set()

        for i in range(len(parking_lots)):  # here I get the geohashes of all the parking lots
            t = list(parking_lots.iloc[i]['geometry'].centroid.coords)
            enc = ghh.encode(t[0][0], t[0][1], precision=6)  # an overapproximation at lvl 6
            current_rect = ghh.rectangle(enc)
            nearby_rect = ghh.neighbours(enc)  # include the nearby neighbors as well for more coverage (can be tuned)
            new_set.add(current_rect['properties']['code'])
            new_set.update(nearby_rect.values())

        # In this first run, test the naive spatial join with geopandas without leveraging the index
        start_time = timeit.default_timer()
        all_pnts = gpd.GeoDataFrame(pnts.apply(lambda x: Point(x['lng'], x['lat']).buffer(1e-10), axis=1),
                                    columns=['geometry'])
        pnts_within = gpd.sjoin(all_pnts, parking_lots, how='inner', op='within')
        elapsed = timeit.default_timer() - start_time

        # In this second run, first filter down the dataframe using the geohashed index
        start_time = timeit.default_timer()
        new_df = pnts.loc[list(new_set)].dropna()
        all_pnts_1 = gpd.GeoDataFrame(new_df.apply(lambda x: Point(x['lng'], x['lat']).buffer(1e-10), axis=1),
                                      columns=['geometry'])
        pnts_within_1 = gpd.sjoin(all_pnts_1, parking_lots, how='inner', op='within')
        elapsed_1 = timeit.default_timer() - start_time

        assert (len(pnts_within_1) == len(pnts_within))
        print(len(pnts) - len(all_pnts_1), "points outside boundaries")
        print((elapsed - elapsed_1) / elapsed * 100, "percent speedup")

        out_file = self.output().path
        gpd.GeoDataFrame(pnts_within_1['geometry']).to_file(out_file)

    def output(self):
        return salted_target(
            self, os.path.join(get_folder_path(), 'data/processed/telematics_points_near_stop' + "-{salt}.csv"))


if __name__ == '__main__':
    test_date_interval = Week.from_date(date(2018, 3, 7))
    create_fake_data = GenerateFakeTelematics(date_interval=test_date_interval)
    spatial_join = SpatialJoinTelematics(date_interval=test_date_interval)

    build([
        create_fake_data, spatial_join
    ], local_scheduler=True)
