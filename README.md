ds-workflow-resume
==============================

Hi there! Thanks for checking out my repo. You can find my Linkedin profile [here](https://www.linkedin.com/in/williammaucla/).

Much of what this repo does is to showcase some of the work I've done as part of Solaria Labs, a Liberty Mutual incubator. You can find more details about Solaria Labs [here](https://www.solarialabs.com).

Many of these files are used for presentations showcasing my work -- please contact me for more details!

Projects
--------

Alternative Data Via Telematics

This project shows how we can leverage telematics data to create valuable insights. [See here for a link](https://wmaucla.bitbucket.io). Check out some of my files in the src/data/altdata folder for some examples!
